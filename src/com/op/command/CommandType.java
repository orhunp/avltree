package com.op.command;

public enum CommandType {
    INSERT,
    DELETE,
    SHOW;

    public enum ShowType {
        INORDER(0),
        PREORDER(1),
        POSTORDER(2);

        private final int value;

        ShowType(final int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
