package com.op.command;

public class Command {
    CommandType type;
    int value;

    public Command(CommandType type) {
        this.type = type;
    }

    public Command(CommandType type, int value) {
        this.type = type;
        this.value = value;
    }

    public CommandType getType() {
        return type;
    }

    public void setType(CommandType type) {
        this.type = type;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Command{" +
                "type=" + type +
                ", value=" + value +
                '}';
    }
}
