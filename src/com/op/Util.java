package com.op;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Locale;

public class Util {
    public static <T> String formatList(List<T> list, char separator) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i != list.size() - 1) {
                result.append(String.format(Locale.US, "%s%c", list.get(i).toString(), separator));
            } else {
                result.append(list.get(i).toString());
            }
        }
        return result.toString();
    }

    public static String readFile(String fileName) {
        StringBuilder result = new StringBuilder();
        try (FileInputStream fileInputStream = new FileInputStream(fileName)) {
            int c;
            while ((c = fileInputStream.read()) != -1) {
                result.append((char) c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    public static void appendToFile(String fileName, String line) {
        try {
            if (!(new File(fileName).exists() || new File(fileName).createNewFile())) {
                throw new IOException("Failed to create output file");
            }
            Files.write(Paths.get(fileName),
                    String.format(Locale.US, "%s\n", line).getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
