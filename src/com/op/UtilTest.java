package com.op;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class UtilTest {

    @Test
    void testFormatList() {
        String[] testArray = {"a", "b", "c", "1", "2", "3"};
        assertEquals("a,b,c,1,2,3", Util.formatList(Arrays.asList(testArray), ','));
        assertEquals("a|b|c|1|2|3", Util.formatList(Arrays.asList(testArray), '|'));

        Integer[] testIntArray = {1, 3, 3, 7};
        assertEquals("1~3~3~7", Util.formatList(Arrays.asList(testIntArray), '~'));
    }

    @Test
    void testReadFile() {
        assertNotEquals(0, Util.readFile("/etc/hostname").length());
    }
}
