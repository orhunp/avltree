package com.op.avltree;

public abstract class Tree {
    int getHeight(Node key) {
        if (key != null)
            return key.height;
        else
            return 0;
    }

    int getBalance(Node key) {
        if (key != null)
            return getHeight(key.right) - getHeight(key.left);
        else
            return 0;
    }

    void updateHeight(Node key) {
        int leftSubtreeHeight = getHeight(key.left);
        int rightSubtreeHeight = getHeight(key.right);
        key.height = Math.max(leftSubtreeHeight, rightSubtreeHeight) + 1;
    }

    Node rotateLeft(Node oldRoot) {
        Node root = oldRoot.right;
        Node temp = root.left;
        root.left = oldRoot;
        oldRoot.right = temp;
        updateHeight(oldRoot);
        updateHeight(root);
        return root;
    }

    Node rotateRight(Node oldRoot) {
        Node root = oldRoot.left;
        Node temp = root.right;
        root.right = oldRoot;
        oldRoot.left = temp;
        updateHeight(oldRoot);
        updateHeight(root);
        return root;
    }

    Node balanceTree(Node root) {
        updateHeight(root);
        int balance = getBalance(root);
        if (balance == 2) {
            if (getBalance(root.right) < 0)
                root.right = rotateRight(root.right);
            return rotateLeft(root);
        }
        if (balance == -2) {
            if (getBalance(root.left) > 0)
                root.left = rotateLeft(root.left);
            return rotateRight(root);
        }
        return root;
    }

    Node findSuccessor(Node root) {
        if (root.left != null)
            return findSuccessor(root.left);
        else
            return root;
    }

    Node insertNode(Node root, int key) {
        if (root == null)
            return new Node(key);
        else if (key < root.value)
            root.left = insertNode(root.left, key);
        else
            root.right = insertNode(root.right, key);
        return balanceTree(root);
    }

    Node removeNode(Node root, int key) {
        if (root == null) {
            return null;
        } else if (key < root.value) {
            root.left = removeNode(root.left, key);
        } else if (key > root.value) {
            root.right = removeNode(root.right, key);
        } else {
            if (root.right == null) {
                root = root.left;
            } else if (root.left == null) {
                root = root.right;
            } else {
                Node temp = findSuccessor(root.right);
                root.value = temp.value;
                root.right = removeNode(root.right, root.value);
            }
        }
        if (root == null)
            return null;
        else
            return balanceTree(root);
    }
}
