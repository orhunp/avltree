package com.op.avltree;

public interface TreeManager {
    void insert(int key);

    void delete(int key);

    int depth(int key);

    Node search(Node root, int key);
}
