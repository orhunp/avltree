package com.op.avltree;

import java.util.List;

public interface TreeCollector {
    void collectInOrder(Node key, List<Integer> collector);

    void collectPreOrder(Node key, List<Integer> collector);

    void collectPostOrder(Node key, List<Integer> collector);
}
