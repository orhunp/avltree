package com.op.avltree;

import java.util.List;

public class AVLTree extends Tree implements TreeManager, TreeCollector {
    public Node root;

    @Override
    public void insert(int key) {
        if (search(root, key) == null)
            root = insertNode(root, key);
    }

    @Override
    public void delete(int key) {
        if (search(root, key) != null)
            root = removeNode(root, key);
    }

    @Override
    public int depth(int key) {
        Node node = search(root, key);
        if (node != null)
            return root.height - node.height;
        return -1;
    }

    @Override
    public Node search(Node root, int key) {
        if (root == null || key == root.value)
            return root;
        else if (key < root.value)
            return search(root.left, key);
        else
            return search(root.right, key);
    }

    @Override
    public void collectPreOrder(Node key, List<Integer> collector) {
        if (key == null) return;
        collector.add(key.value);
        collectPreOrder(key.left, collector);
        collectPreOrder(key.right, collector);
    }

    @Override
    public void collectInOrder(Node key, List<Integer> collector) {
        if (key == null) return;
        collectInOrder(key.left, collector);
        collector.add(key.value);
        collectInOrder(key.right, collector);
    }

    @Override
    public void collectPostOrder(Node key, List<Integer> collector) {
        if (key == null) return;
        collectPostOrder(key.left, collector);
        collectPostOrder(key.right, collector);
        collector.add(key.value);
    }
}
