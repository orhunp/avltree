package com.op;

import com.op.avltree.AVLTree;
import com.op.command.Command;
import com.op.command.CommandType;
import com.op.parser.CommandParser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class App {
    String output;
    ArrayList<Command> commands;
    AVLTree avlTree;
    private static final char SEPARATOR = ',';

    public App(String input, String output) throws IOException {
        this.commands = new CommandParser().parseFile(input);
        this.output = output;
        this.avlTree = new AVLTree();
        File outputFile = new File(output);
        if (outputFile.exists()) {
            if (!outputFile.delete()) {
                throw new IOException("failed to delete file");
            }
        }
    }

    public void run() {
        for (Command command : this.commands) {
            switch (command.getType()) {
                case INSERT:
                    avlTree.insert(command.getValue());
                    break;
                case DELETE:
                    avlTree.delete(command.getValue());
                    break;
                case SHOW:
                    switch (CommandType.ShowType.values()[command.getValue()]) {
                        case INORDER:
                            saveInOrder();
                            break;
                        case PREORDER:
                            savePreOrder();
                            break;
                        case POSTORDER:
                            savePostOrder();
                            break;
                    }
            }
        }
    }

    void saveInOrder() {
        List<Integer> inOrderCollector = new ArrayList<>();
        this.avlTree.collectInOrder(this.avlTree.root, inOrderCollector);
        Util.appendToFile(this.output, Util.formatList(inOrderCollector, SEPARATOR));
    }

    void savePreOrder() {
        List<Integer> preOrderCollector = new ArrayList<>();
        this.avlTree.collectPreOrder(this.avlTree.root, preOrderCollector);
        Util.appendToFile(this.output, Util.formatList(preOrderCollector, SEPARATOR));
    }

    void savePostOrder() {
        List<Integer> postOrderCollector = new ArrayList<>();
        this.avlTree.collectPostOrder(this.avlTree.root, postOrderCollector);
        Util.appendToFile(this.output, Util.formatList(postOrderCollector, SEPARATOR));
    }
}
