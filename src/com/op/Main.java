package com.op;

public class Main {

    public static void main(String[] args) {
        try {
            new App(args[0], args[1]).run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
