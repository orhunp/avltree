package com.op.parser;

import com.op.command.Command;
import com.op.Util;
import com.op.command.CommandType;

import java.util.ArrayList;
import java.util.Locale;

public class CommandParser extends Parser<Command> {
    @Override
    public ArrayList<Command> parseFile(String fileName) {
        ArrayList<Command> commands = new ArrayList<>();
        for (String line : Util.readFile(fileName).split("\n")) {
            String[] fields = line.split("\\s");
            switch (fields[0].toLowerCase(Locale.US)) {
                case "insert":
                    commands.add(new Command(CommandType.INSERT, Integer.parseInt(fields[1])));
                    break;
                case "delete":
                    commands.add(new Command(CommandType.DELETE, Integer.parseInt(fields[1])));
                    break;
                case "show":
                    switch (fields[1].toLowerCase(Locale.US)) {
                        case "inorder":
                            commands.add(new Command(CommandType.SHOW, CommandType.ShowType.INORDER.getValue()));
                            break;

                        case "preorder":
                            commands.add(new Command(CommandType.SHOW, CommandType.ShowType.PREORDER.getValue()));
                            break;

                        case "postorder":
                            commands.add(new Command(CommandType.SHOW, CommandType.ShowType.POSTORDER.getValue()));
                            break;
                    }
                    break;

            }
        }
        return commands;
    }
}
