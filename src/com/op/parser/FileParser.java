package com.op.parser;

import java.util.ArrayList;

public interface FileParser<T> {
    ArrayList<T> parseFile(String fileName);
}
