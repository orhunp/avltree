package com.op;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AppTest {

    @Test
    void testRun() {
        try {
            new App("input.txt", "output.txt").run();
            assertEquals("12,49,78,99\n" +
                    "44,23,12,34,49,45,78,92\n" +
                    "12,23,45,92,78,49,44\n", Util.readFile("output.txt"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
